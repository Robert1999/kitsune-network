import httplib2
import urllib
import os
import webapp2
import json
import cloudstorage
from google.appengine.api import app_identity

import constants
from utilities import send_email, is_production
from entities import Pin
from entities import Credential

class AddPinJsonHandler(webapp2.RequestHandler):
    def post(self):
        pin_dict = {}
        pin_dict["id"] = int(self.request.get('pin_id'))
        pin_dict["icon"] = int(self.request.get('pin_icon'))
        pin_dict["lat"] =  float(self.request.get('pin_lat'))
        pin_dict["lng"] =  float(self.request.get('pin_lon'))

        bucket_name = os.environ.get('BUCKET_NAME',
                           app_identity.get_default_gcs_bucket_name())
        filename = '/' + bucket_name + '/' + constants.PIN_JSON_FILENAME

        with cloudstorage.open(filename) as cloudstorage_file:
            pins_array = json.loads(cloudstorage_file.read())

        pins_array.append(pin_dict)

        with cloudstorage.open(filename, 'w', content_type='text/json') as cloudstorage_file:
            cloudstorage_file.write(json.dumps(pins_array))
        self.response.set_status(200)


class RemovePinJsonHandler(webapp2.RequestHandler):
    def post(self):
        pin_id = int(self.request.get('pin_id'))

        bucket_name = os.environ.get('BUCKET_NAME',
                           app_identity.get_default_gcs_bucket_name())
        filename = '/' + bucket_name + '/' + constants.PIN_JSON_FILENAME

        with cloudstorage.open(filename) as cloudstorage_file:
            pins_array = json.loads(cloudstorage_file.read())

        pin_to_remove = None
        for pin in pins_array:
            if pin['id'] == pin_id:
                pin_to_remove = pin
                break

        if pin_to_remove:
            pins_array.remove(pin_to_remove)
            with cloudstorage.open(filename, 'w', content_type='text/json') as cloudstorage_file:
                cloudstorage_file.write(json.dumps(pins_array))
            self.response.set_status(200)
        else:
            self.response.set_status(404)

class UpdatePinIconJsonHandler(webapp2.RequestHandler):
    def post(self):
        pin_id = int(self.request.get('pin_id'))
        pin_icon = int(self.request.get('pin_icon'))

        bucket_name = os.environ.get('BUCKET_NAME',
                           app_identity.get_default_gcs_bucket_name())
        filename = '/' + bucket_name + '/' + constants.PIN_JSON_FILENAME

        with cloudstorage.open(filename) as cloudstorage_file:
            pins_array = json.loads(cloudstorage_file.read())

        pin_to_update = None
        for pin in pins_array:
            if pin['id'] == pin_id:
                pin_to_update = pin
                break

        if pin_to_update:
            pins_array.remove(pin_to_update)
            pin_to_update['icon'] = pin_icon
            pins_array.append(pin_to_update)
            with cloudstorage.open(filename, 'w', content_type='text/json') as cloudstorage_file:
                cloudstorage_file.write(json.dumps(pins_array))
            self.response.set_status(200)
        else:
            self.response.set_status(404)

class SendDiscordWebHookHandler(webapp2.RequestHandler):
    def post(self):
        if is_production():
            url = Credential.get('DISCORD_WEB_HOOK_URL')
        else:
            url = Credential.get('DISCORD_DEBUG_WEB_HOOK_URL')
        send_discord_web_hook(self, url)

class SendDiscordModerationWebHookHandler(webapp2.RequestHandler):
    def post(self):
        if is_production():
            url = Credential.get('DISCORD_MODERATION_WEB_HOOK_URL')
        else:
            url = Credential.get('DISCORD_DEBUG_WEB_HOOK_URL')
        send_discord_web_hook(self, url)

def send_discord_web_hook(request_handler, web_hook_URL):
    content = request_handler.request.get('message')
    content = content.encode('utf-8', 'ignore')
    data = { 'content': content }
    http = httplib2.Http()
    resp, content = http.request(web_hook_URL, 'POST', urllib.urlencode(data))
    request_handler.response.set_status(resp.status)

class SendEditEmailHandler(webapp2.RequestHandler):
    def post(self):
        email = self.request.get('email')
        access_uuid = self.request.get('access_uuid')
        send_email(email, access_uuid, True)
        self.response.set_status(200)

app = webapp2.WSGIApplication([
    ('/tasks/add_pin_json', AddPinJsonHandler),
    ('/tasks/remove_pin_json', RemovePinJsonHandler),
    ('/tasks/update_pin_json', UpdatePinIconJsonHandler),
    ('/tasks/send_discord_web_hook', SendDiscordWebHookHandler),
    ('/tasks/send_discord_moderation_web_hook', SendDiscordModerationWebHookHandler),
    ('/tasks/send_edit_email', SendEditEmailHandler),
], debug=True)
