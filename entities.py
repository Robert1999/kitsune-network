# -*- coding: utf-8 -*-
import base64
import uuid
import httplib2
import urllib
import json
import logging

from google.appengine.ext import ndb
from google.appengine.api import taskqueue

import constants
from utilities import is_valid_email, is_real_email, is_spam_email, is_production

class JourneyPin(ndb.Model):
    created_datetime = ndb.DateTimeProperty(auto_now_add=True,indexed=True,required=True)
    user_ip_address = ndb.StringProperty(required=True)
    point = ndb.GeoPtProperty(required=True)

    name = ndb.StringProperty(required=True)
    formatted_address = ndb.StringProperty(required=True)
    youtube_id = ndb.StringProperty(required=False)
    reddit_url = ndb.StringProperty(required=False)

    def update_pin_values(self, request_values):
        self.name = request_values.get('name').strip()
        youtube_id = request_values.get('youtube')
        if youtube_id:
            self.youtube_id = youtube_id.strip()
        else:
            self.youtube_id = None
        reddit_url = request_values.get('redditURL')
        if reddit_url:
            self.reddit_url = reddit_url.strip()
        else:
            self.reddit_url = None
        if not self.name:
            raise Exception()

    def set_formatted_address(self, request_values):
        url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}&result_type={}'.format(
            self.point.lat,
            self.point.lon,
            'AIzaSyDuKik7KfT6hIjJ_k6poOKhwy3X0tU6jUE',
            'political'
        )
        http = httplib2.Http()
        resp, content = http.request(url, 'GET')
        response_json = json.loads(content)
        if response_json['status'] == 'ZERO_RESULTS':
            self.formatted_address = ''
        else:
            address_components = response_json['results'][0]['address_components']
            locality = ''
            administrative_area_level_1 = ''
            country = ''
            for component in address_components:
                if 'locality' in component['types']:
                    locality = component['long_name']
                if 'administrative_area_level_1' in component['types']:
                    administrative_area_level_1 = component['short_name']
                if 'country' in component['types']:
                    country = component['long_name']
            formatted_address = ''
            if locality:
                formatted_address += locality
            if administrative_area_level_1:
                formatted_address += ', ' if formatted_address else ''
                formatted_address += administrative_area_level_1
            if country:
                formatted_address += ', ' if formatted_address else ''
                formatted_address += country
            self.formatted_address = formatted_address

    def set_pin_values(self, request_values, remote_addr, is_new_pin):
        password = request_values.get('password')
        if is_production() and password != Credential.get('WONDERFUL_JOURNEY_PASSWORD'):
            raise Exception()
        if is_new_pin:
            latitude = request_values.get('latitude')
            longitude = request_values.get('longitude')
            self.point = ndb.GeoPt(latitude, longitude)
            self.set_formatted_address(request_values)
        self.user_ip_address = remote_addr
        self.update_pin_values(request_values)
        self.put()
        if is_new_pin:
            self.send_discord_web_hook()
        else:
            self.send_discord_moderation_web_hook()

    def send_discord_web_hook(self):
        content = """**New Journey Location!**\n%s\n%s""" % (self.name, self.formatted_address)
        task = taskqueue.add(
            url = '/tasks/send_discord_web_hook',
            params = { 'message': content })

    def send_discord_moderation_web_hook(self):
        content = """**Updated Journey Location**\n%s\n%s""" % (self.name, self.formatted_address)
        task = taskqueue.add(
            url = '/tasks/send_discord_moderation_web_hook',
            params = { 'message': content })


class Pin(ndb.Model):
    created_datetime = ndb.DateTimeProperty(auto_now_add=True,indexed=True,required=True)
    email = ndb.StringProperty(required=True)
    user_ip_address = ndb.StringProperty(required=True)
    access_uuid = ndb.StringProperty(required=True)
    is_activated = ndb.BooleanProperty(required=True,indexed=True,default=False)
    point = ndb.GeoPtProperty(required=True)

    name = ndb.StringProperty(required=True)
    about_you = ndb.TextProperty(required=True)
    pin_icon = ndb.IntegerProperty(required=True)
    favorite_member = ndb.IntegerProperty(required=True)
    favorite_song = ndb.IntegerProperty(required=True)
    communities = ndb.StringProperty(required=True)

    def send_discord_web_hook(self):
        pin_details = 'Title: ' + self.name
        pin_details += '\nFav Song: ' + constants.songs[str(self.favorite_song)]
        pin_details += '\nFav Member: ' + constants.members[str(self.favorite_member)]
        communities = self.communities.split(',')
        if len(communities) > 1 and '0' in communities:
            communities.remove('0')
        pin_details += '\nCommunities: ' + ', '.join([constants.communities[str(community)] for community in communities])
        pin_details += '\nAbout: \n' + self.about_you
        content = """**New Pin Activated!**```%s```""" % pin_details
        task = taskqueue.add(
            url = '/tasks/send_discord_web_hook',
            params = { 'message': content })

    def send_discord_moderation_web_hook(self):
        pin_details = 'ID: ' + str(self.key.id())
        pin_details += '\nIP: ' + self.user_ip_address
        pin_details += '\nEmail: ' + self.email
        pin_details += '\nPin Icon: ' + str(self.pin_icon)
        pin_details += '\nFav Member: ' + str(self.favorite_member)
        pin_details += '\nFav Song: ' + str(self.favorite_song)
        pin_details += '\nTitle: ' + self.name
        pin_details += '\nCommunities: ' + self.communities
        pin_details += '\nAbout: \n' + self.about_you
        content = "** ⛳️ ------------- 🏌️ **".decode('utf8')
        content += "```%s```" % pin_details
        task = taskqueue.add(
            url = '/tasks/send_discord_moderation_web_hook',
            params = { 'message': content })

    @classmethod
    def validate_pin_values(cls, request_values, is_new_pin, translations):
        form_error_message = None
        try:
            name = request_values.get('name').strip()
            about_you = request_values.get('about_you').strip()
            if is_new_pin:
                email = request_values.get('email').strip()
                latitude = request_values.get('latitude')
                longitude = request_values.get('longitude')
                geo_point = ndb.GeoPt(latitude, longitude)
            pin_icon = int(request_values.get('pin_icon'))
            favorite_member = int(request_values.get('favorite_member'))
            favorite_song = int(request_values.get('favorite_song'))
            communities = request_values.get('communities')

            if not pin_icon in constants.pin_icons:
                form_error_message = translations.gettext("All fields are required.")
                return form_error_message
            constants.songs[str(favorite_song)]
            constants.members[str(favorite_member)]
            [constants.communities[str(community)] for community in communities.split(',')]

            if not name or not about_you or (is_new_pin and not email):
                form_error_message = translations.gettext("All fields are required.")
            elif is_new_pin and (not is_valid_email(email) or not is_real_email(email) or is_spam_email(email)):
                form_error_message = translations.gettext("A valid email address is required.")
            elif is_new_pin and Pin.query(Pin.email == email).get():
                form_error_message = translations.gettext("A pin already exists for this email address.")
        except:
            form_error_message = translations.gettext("All fields are required.")

        return form_error_message

    def set_pin_values(self, request_values, remote_addr, is_new_pin, translations):
        if Pin.validate_pin_values(request_values, is_new_pin, translations):
            return
        self.name = request_values.get('name').strip()
        self.about_you = request_values.get('about_you').strip()
        self.pin_icon = int(request_values.get('pin_icon'))
        self.favorite_member = int(request_values.get('favorite_member'))
        self.favorite_song = int(request_values.get('favorite_song'))
        self.communities = request_values.get('communities')
        self.user_ip_address = remote_addr
        if is_new_pin:
            self.access_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')
            self.email = request_values.get('email').strip()
            latitude = request_values.get('latitude')
            longitude = request_values.get('longitude')
            self.point = ndb.GeoPt(latitude, longitude)
        else:
            self.access_uuid = ""
        self.put()

    def delete(self):
        task = taskqueue.add(
            url = '/tasks/remove_pin_json',
            params = { 'pin_id': self.key.id() })
        self.key.delete()

    @classmethod
    def activate_pin(cls, activate_pin_uuid):
        ''' Activate a pin, returns if the activation was a success '''
        pin = Pin.query(Pin.access_uuid == activate_pin_uuid).get()
        if not pin:
            return False
        pin.is_activated = True
        pin.access_uuid = ""
        pin.put()
        task = taskqueue.add(
            url = '/tasks/add_pin_json',
            params = {
                'pin_id': pin.key.id(),
                'pin_icon': pin.pin_icon,
                'pin_lat': pin.point.lat,
                'pin_lon': pin.point.lon,
            })
        pin.send_discord_web_hook()
        return True

class Credential(ndb.Model):
  name = ndb.StringProperty()
  value = ndb.StringProperty()

  @staticmethod
  def get(name):
    NOT_SET_VALUE = "NOT SET"
    retval = Credential.query(Credential.name == name).get()
    if not retval:
      retval = Credential()
      retval.name = name
      retval.value = NOT_SET_VALUE
      retval.put()
    if retval.value == NOT_SET_VALUE:
      logging.error(('Credential %s not found in the database. A placeholder ' +
        'record has been created. Go to the Developers Console for your app ' +
        'in App Engine, look up the Credential record with name=%s and enter ' +
        'its value in that record\'s value field.') % (name, name))
    return retval.value
