import os
import webapp2
import json
import cloudstorage
from google.appengine.api import app_identity

import constants
from entities import Pin

class CreateJsonHandler(webapp2.RequestHandler):
    def get(self):
        bucket_name = os.environ.get('BUCKET_NAME',
                           app_identity.get_default_gcs_bucket_name())
        filename = '/' + bucket_name + '/' + constants.PIN_JSON_FILENAME

        pins_array = []
        for pin in Pin.query(Pin.is_activated == True).fetch():
            pin_dict = {}
            pin_dict["id"] = pin.key.id()
            pin_dict["icon"] = pin.pin_icon
            pin_dict["lat"] = pin.point.lat
            pin_dict["lng"] = pin.point.lon
            pins_array.append(pin_dict)

        with cloudstorage.open(filename, 'w', content_type='text/json') as cloudstorage_file:
            cloudstorage_file.write(json.dumps(pins_array))
        self.response.set_status(200)

app = webapp2.WSGIApplication([
    ('/admin/create_pins_json', CreateJsonHandler),
], debug=True)
