��    7      �  I   �      �  ,   �  "   �       	   
            P   5     �     �     �  	   �     �     �  
   �     �     �     �       
             ,  X   :     �     �  @   �  X   �  ;   Q  7   �  #   �  $   �                    !     &     D     R     ^  ;   j  K   �     �     �  
   	     	  _   ,	     �	      �	  3   �	  &   �	  .   !
     P
  )   l
     �
     �
  �  �
  4   N  -   �  
   �  
   �     �     �  o   �      e  
   �     �     �     �     �     �     �     �     �                     1     ?  ,   �     �  U   �  b   I  I   �  G   �  '   >  2   f     �     �     �     �      �     �     �     �  2      X   3  	   �     �     �     �  �   �     X  $   v  M   �  +   �  F        \  5   {     �     �        !                    "          %   
                  6      1          #   5   2   0         &          4   ,         .      (                 7      3             /      	           '      +       *             )                               -   $       A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-29 15:35-0400
PO-Revision-Date: 2017-06-10 12:08+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
X-Generator: Poedit 2.0.2
 Ein Pin existiert bereits für diese E-Mail-Adresse. Eine gültige Email-Adresse ist erforderlich. Über mich Über dich Pin hinzufügen Alle Felder bitte ausfüllen. Eine E-Mail-Adresse wird benötigt, um Spam zu vermeiden und um dir zu erlauben, den Pin später zu bearbeiten. Eine e-mail wurde dir zugesandt. Schließen Gemeinschaften Gemeinschaft Weiter Neuen Pin erstellen Pin löschen Pin bearbeiten Bearbeite deinen Pin E-Mail E-Mail-Adresse E-mail gesendet Lieblingsmitglied Lieblingslied Zu deiner Sicherheit schlagen wir vor den Pin an einer anderen Stelle zu platzieren und nicht deine richtige Adresse anzugeben. Hilf mit, das Kitsune Network zu übersetzen Symbol Falls die E-Mail nicht in 10 Minuten erscheint, überprüfe bitte deinen Spam-Ordner. Wenn du ein Kitsune (Fan der Band) bist, zögere nicht und trage deinen Standort in die Karte ein! Falls du nicht weißt, wer BABYMETAL sind, dann sieh dir dieses Video an. Falls du etwas zum Projekt beitragen willst, trete unserem Discord bei. Ist der Marker an der richtigen Stelle? Es kann ein paar Minuten dauern, bis es erscheint. Kitsunes Mehr Name Keine Nicht angegliedert an BABYMETAL Pin aktiviert Pin gelöscht Pin aktualisiert Hier bitte keine personenbezogenen Daten eintragen Bittte trete unserem Discord bei, falls du Verbesserungsvorschläge für die Seite hast. Speichern Wähle mindestens eine Wähle einen aus Bestätigungsmail senden Das Ziel dieses Projektes ist es, alle BABYMETAL Fans zu vereinen und eine Ahnung über andere Fans in deiner Nähe zu bekommen. Sie sind alle meine Favoriten Diese Webseite ist auch Open Source! Bitte die E-Mail-Adresse eingeben, um den Pin zu bearbeiten oder zu löschen. Wir sind für neue Vorschläge immer offen. Wir werden deine E-Mail-Adresse auf keinen Fall an Dritte weitergeben. Willkommen im Kitsune Network! Du kannst den Pin verschieben, um ihn zu korrigieren. Dein Pin wurde gelöscht. Dein Pin wurde aktualisiert. 