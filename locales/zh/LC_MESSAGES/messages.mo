��    7      �  I   �      �  ,   �  "   �       	   
            P   5     �     �     �  	   �     �     �  
   �     �     �     �       
             ,  X   :     �     �  @   �  X   �  ;   Q  7   �  #   �  $   �                    !     &     D     R     ^  ;   j  K   �     �     �  
   	     	  _   ,	     �	      �	  3   �	  &   �	  .   !
     P
  )   l
     �
     �
  �  �
  -   U     �  	   �  	   �     �     �  ]   �  !   =     _     f     m     t     {     �     �     �     �     �     �     	       `   /     �     �  F   �  `   �  G   [  E   �  !   �  -        9     B     O     V     Z     q     �     �  !   �  W   �     -     4     G     T  w   g     �     �  N     !   f  6   �     �  0   �  !        1        !                    "          %   
                  6      1          #   5   2   0         &          4   ,         .      (                 7      3             /      	           '      +       *             )                               -   $       A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-29 15:35-0400
PO-Revision-Date: 2017-05-30 19:19+0200
Last-Translator: 
Language: zh
Language-Team: zh <LL@li.org>
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
X-Generator: Poedit 2.0.2
 此邮件地址已存在一个位置标注。 需要有效的邮箱地址。 关于我 关于你 添加位置标注 所有必填字段。 需要邮件地址来避免垃圾信息，并使你在之后能够编辑你的位置标注。 邮件已发送至你的邮箱。 关闭 社区 社区 继续 创建一个新的位置标注 删除位置标注 编辑位置标注 编辑你的位置标注 电子邮件 电子邮件地址 邮件以发送 最喜欢的成员 最喜欢的歌曲 为了你的安全，建议将位置标注到区别于你所处的实际位置的其他位置。 帮助翻译 Kitsune 网络 图标 如果在 10 分钟后仍未到邮件，请检查你的垃圾邮箱。 如果你是 kitsune （这个乐队的粉丝）请放松地在地图上标注自己的位置！ 如果你不知 BABYMETAL 是谁，去看看关于她们的视频吧。 如果你愿意贡献自己的力量，请在 Discord 加入我们。 标记所在的位置正确吗？ 收到邮件可能需要几分钟的等待。 Kitsunes 更多内容 称呼 无 不附属于 BABYMETAL 位置标注已激活 位置标注已删除 位置标注已更新 请避免在此透露个人信息 如果你有任何关于这个网站功能的建议，请在 Discord 上加入我们。 保存 至少选择一个 选择一个 发送激活邮件 这个项目的目标是联合所有的 BABYMETAL 粉丝并使你对你所在地区的粉丝有一个大致的了解。 她们都是我的最爱 这个网站也是开源的！ 想要编辑或删除你的位置标注，在下方输入你的邮件地址。 我们乐于接受新的建议。 我们不会将你的邮件地址透露给任何人。 欢迎来到 Kitsune 网络！ 你可以拖动位置标注到正确的位置。 你的位置标注已被删除。 你的位置标注已更新。 