��    7      �  I   �      �  ,   �  "   �       	   
            P   5     �     �     �  	   �     �     �  
   �     �     �     �       
             ,  X   :     �     �  @   �  X   �  ;   Q  7   �  #   �  $   �                    !     &     D     R     ^  ;   j  K   �     �     �  
   	     	  _   ,	     �	      �	  3   �	  &   �	  .   !
     P
  )   l
     �
     �
  �  �
  ?   b     �     �     �  
   �     �  Z   �  "   K     n     s     {     �     �     �     �     �     �     �     �     �       c     .   t     �  G   �  ]   �  ;   N  8   �     �  0   �                     %     +     E     V     f  =   x  H   �     �                 o   0     �  %   �  N   �  #   .  9   R     �  7   �      �  "           !                    "          %   
                  6      1          #   5   2   0         &          4   ,         .      (                 7      3             /      	           '      +       *             )                               -   $       A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-29 15:35-0400
PO-Revision-Date: 2017-05-29 23:15+0200
Last-Translator: 
Language: nb_NO
Language-Team: nb_NO <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
X-Generator: Poedit 2.0.2
 Det er allerede registrert en markør på denne e-postadressen. En gyldig e-post kreves. Om Meg Om Deg Ny Markør Alle feltene må fylles ut. En e-postadresse kreves for å hindre spam og for å la deg redigere markøren din senere. En e-post har blitt sendt til deg. Lukk Samfunn Samfunn Fortsett Lag En Ny Markør Fjern Markør Rediger Markør Rediger Markøren Din E-post E-postadresse E-post Sendt Yndlingsmedlem Yndlingslåt Av sikkerhetsgrunner foreslår vi at du plasserer markøren din på et annet sted enn adressen din. Hjelp til med oversettelsen av Kitsune Network Ikon Dersom den ikke kommer frem innen ti minutter, sjekk søppelposten din. Ikke nøl med å legge til din plassering på kartet dersom du er en kitsune (fan av bandet)! Dersom du ikke vet hvem BABYMETAL er, se på denne videoen. Dersom du vil bidra, vennligst bli med i Discorden vår. Er markøren på riktig plass? Det kan ta et par minutter før den kommer frem. Kitsuner Mer Navn Ingen Ikke tilknyttet BABYMETAL Markør Aktivert Markør Fjernet Markør Oppdatert Vennligst ikke legg inn personidentifiserende informasjon her Vennligst bli med i Discorden vår om du har noen forslag til nettsiden. Lagre Velg Minst En Velg En Send Aktivasjonslink Målet med dette prosjektet er å forene alle BABYMETAL-fans og å gi deg en pekepinn på fans i området ditt. Alle er favorittene mine Denne nettsiden er også open-source! Skriv inn e-postadressen din under for å redigere eller fjerne markøren din. Vi er alltid åpne for nye forslag. Vi kommer aldri til å dele e-postadressen din med andre. Velkommen til Kitsune Network! Du kan dra på markøren for å korrigere plasseringen. Markøren din har blitt fjernet. Markøren din har blitt oppdatert. 