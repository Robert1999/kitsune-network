# Translations template for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-05-29 15:35-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: entities.py:73 entities.py:80 entities.py:86
msgid "All fields are required."
msgstr ""

#: entities.py:82
msgid "A valid email address is required."
msgstr ""

#: entities.py:84
msgid "A pin already exists for this email address."
msgstr ""

#: templates/email_sent.html:2
msgid "Email Sent"
msgstr ""

#: templates/email_sent.html:5
msgid "An email has been sent to you."
msgstr ""

#: templates/email_sent.html:7
msgid "It may take a few minutes to appear."
msgstr ""

#: templates/email_sent.html:9
msgid "If it does not appear within 10 minutes, check your spam folder."
msgstr ""

#: templates/email_sent.html:13 templates/map.html:30 templates/map.html:86
#: templates/pin/pin_form.html:64 templates/pin_deleted.html:7
#: templates/pin_updated.html:7
msgid "Close"
msgstr ""

#: templates/map.html:27
msgid "Pin Activated"
msgstr ""

#: templates/map.html:41
msgid "Is the marker in the correct place?"
msgstr ""

#: templates/map.html:42
msgid "You can drag the pin to make corrections."
msgstr ""

#: templates/map.html:43
msgid ""
"For your safety we suggest placing your pin on something other than your "
"actual address."
msgstr ""

#: templates/map.html:46
msgid "Continue"
msgstr ""

#: templates/map.html:54
msgid "Welcome to Kitsune Network!"
msgstr ""

#: templates/map.html:55
msgid ""
"The aim of this project is to unite all BABYMETAL fans and to get an idea"
" of fans in your area."
msgstr ""

#: templates/map.html:57
msgid "If you do not know who BABYMETAL are then watch this video."
msgstr ""

#: templates/map.html:59
msgid ""
"If you are a kitsune (fan of the band) please feel free to add your "
"location on the map!"
msgstr ""

#: templates/map.html:60
msgid ""
"Please join us on Discord if you have any feature suggestions for the "
"site."
msgstr ""

#: templates/map.html:61
msgid "We are always open to new suggestions."
msgstr ""

#: templates/map.html:62
msgid "This website is open source too!"
msgstr ""

#: templates/map.html:63
msgid "If you'd like to contribute, please join us on Discord."
msgstr ""

#: templates/map.html:64
msgid "Not affiliated with BABYMETAL"
msgstr ""

#: templates/map.html:75
msgid "Help translate Kitsune Network"
msgstr ""

#: templates/map.html:79
msgid "To edit or delete your pin, enter your email below."
msgstr ""

#: templates/map.html:82
msgid "Email"
msgstr ""

#: templates/map.html:83
msgid "Edit Pin"
msgstr ""

#: templates/map.html:94
msgid "Add Pin"
msgstr ""

#: templates/map.html:95
msgid "More"
msgstr ""

#: templates/map.html:96
msgid "Kitsunes"
msgstr ""

#: templates/pin_deleted.html:1
msgid "Pin Deleted"
msgstr ""

#: templates/pin_deleted.html:4
msgid "Your pin has been deleted."
msgstr ""

#: templates/pin/pin_form.html:22 templates/pin_info_window.html:3
msgid "Favorite Member"
msgstr ""

#: templates/pin/pin_form.html:33 templates/pin_info_window.html:5
msgid "Favorite Song"
msgstr ""

#: templates/pin_info_window.html:8
msgid "Communities"
msgstr ""

#: templates/pin/pin_form.html:44 templates/pin_info_window.html:15
msgid "Community"
msgstr ""

#: templates/pin_info_window.html:18
msgid "About Me"
msgstr ""

#: templates/pin_updated.html:1
msgid "Pin Updated"
msgstr ""

#: templates/pin_updated.html:4
msgid "Your pin has been updated."
msgstr ""

#: templates/pin/edit_pin_form.html:2
msgid "Edit Your Pin"
msgstr ""

#: templates/pin/edit_pin_form.html:19
msgid "Delete Pin"
msgstr ""

#: templates/pin/edit_pin_form.html:21
msgid "Save"
msgstr ""

#: templates/pin/new_pin_form.html:2
msgid "Create A New Pin"
msgstr ""

#: templates/pin/new_pin_form.html:5
msgid "Email Address"
msgstr ""

#: templates/pin/new_pin_form.html:7
msgid ""
"An email address is required to avoid spam and allow you to edit your pin"
" later."
msgstr ""

#: templates/pin/new_pin_form.html:7
msgid "We'll never share your email with anyone else."
msgstr ""

#: templates/pin/new_pin_form.html:10
msgid "Send Activation Email"
msgstr ""

#: templates/pin/pin_form.html:4
msgid "Name"
msgstr ""

#: templates/pin/pin_form.html:8
msgid "Icon"
msgstr ""

#: templates/pin/pin_form.html:24 templates/pin/pin_form.html:35
msgid "Select One"
msgstr ""

#: templates/pin/pin_form.html:25 templates/pin/pin_form.html:36
msgid "They are all my favorite"
msgstr ""

#: templates/pin/pin_form.html:46
msgid "Select At Least One"
msgstr ""

#: templates/pin/pin_form.html:47
msgid "None"
msgstr ""

#: templates/pin/pin_form.html:55
msgid "About You"
msgstr ""

#: templates/pin/pin_form.html:56
msgid "Please do not put any personal identifying information here"
msgstr ""

