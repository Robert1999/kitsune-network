��    9      �  O   �      �  ,   �  "        9  	   B     L     T  P   m     �     �     �  	   �     �       
             '     5     ;  
   I     T     d  X   r     �     �  @   �  X   0  ;   �  7   �  #   �  /   !  $   Q     v          �     �     �     �     �     �  ;   �  K   	     Z	     _	  
   s	     ~	  _   �	     �	      
  3   .
  &   b
  .   �
     �
  )   �
     �
          6  �  Q  /   �  1     	   C     M     Y  #   g  k   �      �            
   +  	   6     @     R  
   ^     i     w     }     �     �     �  b   �  "   "     E  E   L  i   �  C   �  B   @     �  8   �  ,   �                         !     ?     K     X  8   g  X   �     �                %  b   @      �  $   �  8   �  *   "  1   M       -   �     �     �     �     &   
                      *                      0      $      !      %          9                               "   6      '   2   #             	   7                    5       (          3                 ,   .   )                    1   8       +      4   /   -                   A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear on the map. It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been activated! Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-28 12:20-0400
PO-Revision-Date: 2017-05-29 00:45+0200
Language: pt
Language-Team: pt <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
Last-Translator: 
X-Generator: Poedit 2.0.2
 Um pin já existe para este endereço de email. É necessário ter um endereço de email válido. Sobre Mim Sobre Você Adicionar Pin Todos os campos são obrigatórios. Um endereço de email é necessário para evitar spam e permitir que você possa editar seu pin mais tarde. Um email foi enviado para você. Fechar Comunidades Comunidade Continuar Criar Um Novo Pin Deletar Pin Editar Pin Edite seu pin Email Endereço de Email Email Enviado Membro Favorito Música Favorita Para sua segurança, sugerimos que você coloque o seu pin em um lugar diferente do seu endereço. Ajude a traduzir o Kitsune Network Ícone Se ele não chegar dentro de 10 minutos, verifique sua pasta de spam. Se você é um kitsune (fã da banda), por favor sinta-se livre para adicionar sua localização no mapa! Se você não sabe quem são BABYMETAL, então assista este vídeo. Se você quiser contribuir, por favor, junte-se a nós no Discord. O pin está no lugar correto? Pode levar alguns minutos para que ele apareça no mapa. Ele pode levar alguns minutos para aparecer. Kitsunes Mais Nome Nenhum Sem afiliação com BABYMETAL Pin Ativado Pin Deletado Pin Atualizado Por favor, não insira nenhuma informação pessoal aqui Por favor, junte-se a nós no Discord se você tiver sugestões de recursos para o site. Salvar Selecione pelo menos um Selecione Um Enviar Email de Ativação O objetivo deste projeto é unir todos os fãs de BABYMETAL e ter uma ideia dos fãs na sua área. Todas elas são minhas favoritas Este website é open source também! Para editar ou deletar seu pin, insira seu email abaixo. Estamos sempre abertos a novas sugestões. Nunca iremos compartilhar seu email com ninguém. Bem-vindo ao Kitsune Network! Você pode arrastar o pin para posicioná-lo. Seu pin foi ativado! Seu pin foi deletado. Seu pin foi atualizado. 