��    7      �  I   �      �  ,   �  "   �       	   
            P   5     �     �     �  	   �     �     �  
   �     �     �     �       
             ,  X   :     �     �  @   �  X   �  ;   Q  7   �  #   �  $   �                    !     &     D     R     ^  ;   j  K   �     �     �  
   	     	  _   ,	     �	      �	  3   �	  &   �	  .   !
     P
  )   l
     �
     �
  �  �
  %   \  !   �     �     �     �     �  b   �     I     g  	   m  	   w     �     �  	   �     �     �     �     �     �     �     �  Y   �  .   R     �  C   �  n   �  :   ;  S   v  .   �  "   �       
   $     /  	   4  "   >     a     q     �  4   �  O   �          &     <     M  ~   b     �  &   �  I   "  /   l  ;   �  "   �  /   �     +     C        !                    "          %   
                  6      1          #   5   2   0         &          4   ,         .      (                 7      3             /      	           '      +       *             )                               -   $       A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-29 15:35-0400
PO-Revision-Date: 2017-05-29 23:21+0200
Last-Translator: 
Language: id
Language-Team: id <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
X-Generator: Poedit 2.0.2
 Pin sudah ada untuk alamat email ini. Diperlukan alamat Email yang sah. Tentang saya Tentang anda Tambahkan Pin Semua kolom harap di isi. Alamat Email diperlukan untuk menghindari spam dan mengijinkan anda untuk mengubah pin anda nanti. Email sudah terkirim ke anda. Tutup Komunitas Komunitas Lanjut Buat Pin baru Hapus Pin Koreksi Pin Ubah Pin anda Email Alamat Email Email terkirim Member Favorit Lagu Favorit Untuk keamanan anda, kami sarankan untuk meletakkan Pin anda tidak pada alamat asli anda. Bantu kami untuk menerjemahkan Kitsune Network Simbol Jika Email tidak muncul dalam waktu 10 menit, cek folder spam anda. Jika kamu adalah seorang Kitsune (Penggemar band ini) Jangan sungkan untuk menambahkan lokasimu ke dalam peta! Jika kamu tidak tahu siapa itu BABYMETAL tonton video ini. Jika kamu ingin berkontribusi, jangan sungkan untuk gabung bersama kita di Discord. Apakah penandanya berada di tempat yang benar? Butuh beberapa menit untuk muncul. Kitsune Selebihnya Nama Tidak ada Tidak berhubungan dengan BABYMETAL Pin sudah aktif Pin telah dihapus Pin telah diperbaharui Tolong jangan memberi identitas personal anda disini Tolong bergabung bersama kami di Discord jika kamu punya saran untuk situs ini. Simpan Pilih setidaknya satu Pilih salah satu Kirim Email aktivasi Tujuan proyek ini adalah untuk menyatukan semua fans BABYMETAL dan untuk mengetahui lokasi fans yang berada di area sekitarmu. Mereka semua favorit saya Website ini bersifat open source juga! Untuk mengubah atau menghapus Pin anda, masukkan Email anda dibawah sini. Kami selalu terbuka untuk sebuah saran yg baru. Kami tidak akan pernah membagikan email anda ke orang lain. Selamat datang di Kitsune Network! Anda bisa menarik Pin-nya untuk mengkoreksinya. Pin anda telah dihapus. Pin anda telah diperbaharui. 