��    7      �  I   �      �  ,   �  "   �       	   
            P   5     �     �     �  	   �     �     �  
   �     �     �     �       
             ,  X   :     �     �  @   �  X   �  ;   Q  7   �  #   �  $   �                    !     &     D     R     ^  ;   j  K   �     �     �  
   	     	  _   ,	     �	      �	  3   �	  &   �	  .   !
     P
  )   l
     �
     �
  �  �
  =   \  ,   �  	   �     �     �      �  `     (   h     �     �  	   �  	   �     �     �  
   �     �     �               5     F  P   X  #   �     �  J   �  b     1   �  ;   �  )   �  (        B     K     P     W     _     y     �     �  1   �  Q   �     (     0     H  )   W  t   �     �  2     E   A  .   �  6   �  "   �  4        E     _        !                    "          %   
                  6      1          #   5   2   0         &          4   ,         .      (                 7      3             /      	           '      +       *             )                               -   $       A pin already exists for this email address. A valid email address is required. About Me About You Add Pin All fields are required. An email address is required to avoid spam and allow you to edit your pin later. An email has been sent to you. Close Communities Community Continue Create A New Pin Delete Pin Edit Pin Edit Your Pin Email Email Address Email Sent Favorite Member Favorite Song For your safety we suggest placing your pin on something other than your actual address. Help translate Kitsune Network Icon If it does not appear within 10 minutes, check your spam folder. If you are a kitsune (fan of the band) please feel free to add your location on the map! If you do not know who BABYMETAL are then watch this video. If you'd like to contribute, please join us on Discord. Is the marker in the correct place? It may take a few minutes to appear. Kitsunes More Name None Not affiliated with BABYMETAL Pin Activated Pin Deleted Pin Updated Please do not put any personal identifying information here Please join us on Discord if you have any feature suggestions for the site. Save Select At Least One Select One Send Activation Email The aim of this project is to unite all BABYMETAL fans and to get an idea of fans in your area. They are all my favorite This website is open source too! To edit or delete your pin, enter your email below. We are always open to new suggestions. We'll never share your email with anyone else. Welcome to Kitsune Network! You can drag the pin to make corrections. Your pin has been deleted. Your pin has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-29 15:35-0400
PO-Revision-Date: 2017-06-10 12:11+0200
Last-Translator: 
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
X-Generator: Poedit 2.0.2
 Ya existe un pin para esta dirección de correo electrónico. Un correo electrónico válido es requerido. Sobre mí Sobre ti Añadir pin Todos los campos son requeridos. Un correo electrónico es requerido para evitar spam y para permitirte editar tu pin más tarde. Se te ha enviado un correo electrónico. Cerrar Comunidades Comunidad Continúa Crear un nuevo pin Eliminar pin Editar pin Editar tu pin Correo electrónico Correo electrónico Correo electrónico enviado Miembro favorito Canción favorita Por tu seguridad, sugerimos colocar el pin en un lugar que no sea tu dirección. Ayuda a traducir la Kitsune Network Icono Si no te ha llegado en 10 minutos, revisa tu Carpeta de Correo No Deseado. ¡Si eres un kitsune (fan de la banda) por favor siéntete libre de añadir tu ubicación al mapa! Si no sabes quién es BABYMETAL, mira este video. Si te gustaría contribuir, por favor únetenos en Discord. ¿El marcador está en el lugar correcto? Podría tardar unos minutos en aparecer. Kitsunes Más Nombre Ninguna No afiliado con BABYMETAL Pin Activado Pin eliminado Pin actualizado Por favor no coloques información personal aquí Por favor únetenos en Discord si tienes alguna sugerencia para mejorar el sitio. Guardar Selecciona al menos una Selecciona una Enviar correo electrónico de activación El objetivo de este proyecto es unir a todos los fans de BABYMETAL y tener una idea de los fans que hay en tu área. Todas son mis favoritas ¡Esta página web también es de código abierto! Para editar o borrar tu pin, introduce tu correo electrónico debajo. Siempre estamos abiertos a nuevas sugerencias. Jamás compartiremos tu correo electrónico con nadie. ¡Bienvenido a la Kitsune Network! Puedes arrastrar el pin para corregir la ubicación. Su pin ha sido eliminado. Tu pin ha sido actualizado. 