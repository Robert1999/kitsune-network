var map, newPinMarker;

function initMap(startLoc, didLoadHandler) {
  mapboxgl.accessToken = 'pk.eyJ1Ijoid2VybTA5OCIsImEiOiJjamRreW1xazAwMHhjMzNvMm1wNDJvMHpuIn0.1b9hXdk6zyunu3OJC5wreA';
  map = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/werm098/cjdkzvf9e07xn2snksdhmygv5?optimize=true', //hosted style id
      center: [startLoc.lng, startLoc.lat], // starting position
      // localIdeographFontFamily: "'Noto Sans', 'Noto Sans CJK SC', sans-serif",
      zoom: 3 // starting zoom
  });
  map.dragRotate.disable();
  map.touchZoomRotate.disableRotation();
  map.addControl(new mapboxgl.FullscreenControl());
  map.addControl(new mapboxgl.NavigationControl({showCompass: false}));
  var geolocateControl = new mapboxgl.GeolocateControl({showUserLocation: false, fitBoundsOptions: {maxZoom: 7}});
  geolocateControl.on('error', function(e) {
    alert("Error: " + e.message);
  });
  map.addControl(geolocateControl);
  map.on('zoomstart', removeAllPopups);
  map.on('load', function() {
    loadImages("images/pins/journey/", "journey", 0, 2, function() {
      loadImages("images/pins/", "kitsune", 1, 8, function() {
        didLoadHandler();
      });
    });
  });
}

function flyTo(lngLat, zoom) {
  map.flyTo({center: lngLat, zoom: zoom});
}

function removeAllPopups() {
  if (newPinMarker !== undefined) {
    return;
  }
  document.querySelectorAll('.mapboxgl-popup').forEach(function(el) {
    el.parentNode.removeChild(el);
  });
}

function showPopup(lngLat, html) {
  removeNewPinMarker();
  removeAllPopups();
  new mapboxgl.Popup({anchor: "bottom", offset: (map.getZoom() > 7 ? 50 : 20)})
    .setLngLat(lngLat)
    .setHTML(html)
    .addTo(map);
}

function setVisibility(layer, isVisible) {
  map.setLayoutProperty(layer, 'visibility', isVisible ? 'visible' : 'none');
}

function setupKitsunePins(pins) {
  var features = Array();
  pins.forEach(function(pin) {
    var lngLat = [pin.lng, pin.lat];
    features.push({
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": lngLat
        },
        "properties": {
          "id": pin.id,
          "image": pin.icon
        }
    });
  });
  map.addLayer({
      "id": "kitsunes",
      "type": "symbol",
      "source": {
          "type": "geojson",
          "data": {
              "type": "FeatureCollection",
              "features": features
          }
      },
      "layout": {
          "icon-allow-overlap": true,
          "icon-image": "kitsune-{image}",
          "icon-anchor": "bottom",
          "icon-size": {
            "stops": [
                  [6, 0.5],
                  [8, 1]
              ]
          }
      }
  });
  map.on('click', 'kitsunes', function (e) {
    var feature = e.features[0];
      getKitsunePinContent(feature.properties.id, function(result) {
        showPopup(feature.geometry.coordinates, result);
      })
  });
  map.on('mouseenter', 'kitsunes', function () {
      map.getCanvas().style.cursor = 'pointer';
  });
  map.on('mouseleave', 'kitsunes', function () {
      map.getCanvas().style.cursor = '';
  });
}

function loadImages(basePath, prefix, index, endIndex, completion) {
  map.loadImage(basePath + index + ".png?v=" + versionIdentifier(), function(error, image) {
      if (error) throw error;
      map.addImage(prefix + "-" + index, image);
      if (index < endIndex) {
        loadImages(basePath, prefix, index + 1, endIndex, completion);
      } else {
        completion();
      }
  });
}

function getNewPinLatLng() {
  return newPinMarker.getLngLat();
}

function removeNewPinMarker() {
  if (newPinMarker !== undefined) {
    newPinMarker.remove();
  }
}

function dropPin(isJourney) {
  map.zoomTo(12);
  removeNewPinMarker();
  removeAllPopups();

  var contentID = isJourney ? 'newJourneyPinMessage' : 'newPinMessage'
  var popup = new mapboxgl.Popup({closeOnClick: false, anchor: "bottom", offset: 35})
    .setHTML(document.getElementById(contentID).innerHTML);
  newPinMarker = new mapboxgl.Marker()
    .setLngLat(map.getCenter())
    .setPopup(popup)
    .addTo(map);
  popup.once('close', function() {
    newPinMarker.remove();
  })
  newPinMarker.togglePopup();
  var onDown = function(e) {
    e.preventDefault();
    map.dragPan.disable();
    var onMove = function(e) {
      newPinMarker.setLngLat(e.lngLat);
    }
    var onMoved = function(e) {
      map.dragPan.enable();
      map.off('mousemove', onMove);
      map.off('touchmove', onMove);
    }
    map.on('mousemove', onMove);
    map.on('touchmove', onMove);
    map.once('mouseup', onMoved);
    map.once('touchend', onMoved);
  }
  newPinMarker.getElement().addEventListener('mousedown',  onDown);
  newPinMarker.getElement().addEventListener('touchstart',  onDown);
}

function setupJourneyLayer(json) {
  setupJourneyLines(json);
  var features = Array();
  json.forEach(function(pin, index) {
    var lngLat = [pin.lng, pin.lat];
    var newLocation = document.getElementById('wonderfulJourneyLocation' + index);
    newLocation.onclick = function() {
      removeNewPinMarker();
      flyTo(lngLat, 9);
      getJourneyPinContent(pin.id, function(html) {
        showPopup(lngLat, html)
      });
    };
    features.push({
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": lngLat
        },
        "properties": {
          "id": pin.id,
          "image": index % 3
        }
    });
  });
  map.addLayer({
      "id": "journey",
      "type": "symbol",
      "source": {
          "type": "geojson",
          "data": {
              "type": "FeatureCollection",
              "features": features
          }
      },
      "layout": {
          "icon-allow-overlap": true,
          "icon-image": "journey-{image}",
          "icon-size": {
            "stops": [
                  [6, 0.5],
                  [8, 1]
              ]
          }
      }
  });
  map.on('click', 'journey', function (e) {
    var feature = e.features[0];
    getJourneyPinContent(feature.properties.id, function(result) {
      showPopup(feature.geometry.coordinates, result);
    });
  });
  map.on('mouseenter', 'journey', function () {
      map.getCanvas().style.cursor = 'pointer';
  });
  map.on('mouseleave', 'journey', function () {
      map.getCanvas().style.cursor = '';
  });
}

function setupJourneyLines(json) {
  var coords = Array();
  json.forEach(function(pin, index) {
    var lngLat = [pin.lng, pin.lat];
    coords.push(lngLat);
  });
  map.addLayer({
    'id': 'journey-lines',
    'type': 'line',
    'source': {
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': coords
                }
            }]
        }
    },
    'paint': {
        'line-color': '#e6df85',
        'line-width': 4,
        'line-opacity': .8
    }
  });
}

function toggleWonderfulJourney() {
  removeAllPopups();
  var isAlreadyVisible = map.getLayoutProperty('journey', 'visibility') == 'visible';
  setVisibility('journey', !isAlreadyVisible);
  setVisibility('journey-lines', !isAlreadyVisible);
  setJourneyFilterSelected(!isAlreadyVisible);
}

function toggleKitsuneMarkers() {
  removeAllPopups();
  var isAlreadyVisible = map.getLayoutProperty('kitsunes', 'visibility') == 'visible';
  setVisibility('kitsunes', !isAlreadyVisible);
  setKitsunePinsFilterSelected(!isAlreadyVisible);
}
