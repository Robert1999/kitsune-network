function loadWonderfulJourney() {
  ajax({
    method: 'GET',
    relativeURL: '/journey/journey_pins.json',
    dataType: 'JSON',
    success: function(json) {
      var filterOption = document.getElementById('wonderfulJourneyOption');
      json.forEach(function(pin, index) {
        var newLocation = document.getElementById('wonderfulJourneyLocationTemplate').cloneNode(true);
        newLocation.id = "wonderfulJourneyLocation" + index;
        newLocation.classList.add("wonderfulJourneyLocation");
        newLocation.children[0].innerHTML = pin.title;
        newLocation.children[1].innerHTML = pin.location;
        filterOption.insertAdjacentElement('afterend', newLocation);
      });
      setupJourneyLayer(json);
    },
    error: function() {
      // There was a connection error of some sort
    }
  });
}

function getJourneyPinContent(pinID, completion) {
  ajax({
    method: 'GET',
    relativeURL: '/journey/pin/' + pinID,
    dataType: 'HTML',
    success: function(html) {
      if (document.getElementById('controlAddJourneyPinButton')) {
        html = "<a onclick='openJourneyModalForm(" + pinID + "); return false;'>Edit Journey Pin</a>" + html;
      }
      completion(html);
    },
    error: function() {
      // There was a connection error of some sort
    }
  });
}

function setJourneyFilterSelected(isSelected) {
  var classList = document.getElementById('wonderfulJourneyOption').classList;
  if (isSelected) {
    classList.add('active');
  } else {
    classList.remove('active');
  }
  document.querySelectorAll('.wonderfulJourneyLocation').forEach(function(el) {
    if (isSelected) {
      el.style.display = "block";
    } else {
      el.style.display = "none";
    }
  });
}

function openJourneyModalForm(editJourneyPinID) {
  ajax({
    method: 'GET',
    relativeURL: '/journey/journey_update_pin_form/' + (editJourneyPinID !== undefined ? editJourneyPinID : ""),
    dataType: 'HTML',
    success: function(html) {
      openModal(html);
      if (editJourneyPinID !== undefined) {
        document.getElementById('inputEditJourneyPinID').value = editJourneyPinID        
      }
    },
    error: function() {
      // There was a connection error of some sort
    }
  });
}

function getJourneyPinFormData() {
  var data = new QueryStringBuilder();
  data.add("editJourneyPinID", document.getElementById('inputEditJourneyPinID').value);
  data.add("name", document.getElementById('inputName').value);
  data.add("password", document.getElementById('inputPassword').value);
  if (newPinMarker !== undefined) {
    var latlng = getNewPinLatLng();
    data.add("latitude", latlng.lat);
    data.add("longitude", latlng.lng);
  }
  data.add("redditURL", document.getElementById('inputRedditURL').value);
  var youtubeUrl = document.getElementById('inputYT').value;
  if (youtubeUrl != "") {
    var index = youtubeUrl.indexOf('=');
    var youtubeID = youtubeUrl.substring(youtubeUrl.indexOf('=') + 1);
    data.add("youtube", youtubeID);
  }
  return data;
}

function submitJourneyPinForm() {
  var data = getJourneyPinFormData();
  var imageFile = document.getElementById("inputImage").files[0];
  if (imageFile === undefined) {
    if (document.getElementById('inputEditJourneyPinID').value.length == 0) {
      var formErrorElement = document.getElementById("formError");
      formErrorElement.innerHTML = "Missing Image.";
    } else {
      submitJourneyPinFormData(data);
    }
    return;
  }
  var reader = new FileReader();
  reader.onload = function(readerEvt) {
    var binaryString = readerEvt.target.result;
    data.add("image", btoa(binaryString));
    submitJourneyPinFormData(data);
  };
  reader.readAsBinaryString(imageFile);
}

function submitJourneyPinFormData(data) {
  ajax({
    method: document.getElementById('inputEditJourneyPinID').value > 0 ? 'PUT' : 'POST',
    relativeURL: '/journey',
    dataType: 'HTML',
    data: data,
    success: function(html) {
      removeNewPinMarker();
      openModal(html);
    },
    error: function(html) {
      var formErrorElement = document.getElementById("formError");
      formErrorElement.innerHTML = html;
    }
  });
}
