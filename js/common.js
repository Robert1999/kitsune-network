function versionIdentifier() {
  return document.getElementById("versionIdentifier").innerHTML;
}

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

function ajax(settings) {
  showLoadingIndicator();
  var request = new XMLHttpRequest();
  request.open(settings.method, settings.relativeURL, true);
  if (settings.method == 'POST' || settings.method == 'PUT') {
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  }
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      if (settings.dataType == "JSON") {
        var json = JSON.parse(request.responseText);
        settings.success(json)
      } else {
        settings.success(request.responseText)
      }
    } else {
      // We reached our target server, but it returned an error
      settings.error(request.responseText);
    }
    hideLoadingIndicator();
  };
  request.onerror = function() {
    settings.error("");
    hideLoadingIndicator();
  };
  if (settings.method == 'POST' || settings.method == 'PUT') {
    request.send(settings.data.toQueryString());
  } else {
    request.send();
  }
}

function QueryStringBuilder() {
  var nameValues = [];
  this.add = function(name, value) {
    nameValues.push( {name: name, value: value} );
  };
  this.toQueryString = function() {
    var segments = [], nameValue;
    for (var i = 0, len = nameValues.length; i < len; i++) {
        nameValue = nameValues[i];
        segments[i] = encodeURIComponent(nameValue.name) + "=" + encodeURIComponent(nameValue.value);
    }
    return segments.join("&");
  };
}
