var map, newPinMarker, markerInfoWindow;
var startZoom = 5;
var wonderfulJourneyMarkers = Array();
var journeyPath;
var kitsuneMarkers = Array();

function initMap(startLoc, didLoadHandler) {
  map = new google.maps.Map(document.getElementById('map'), {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true,
    center: startLoc,
    zoom: startZoom,
    styles:
     [{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"administrative.neighborhood","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#c50000"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#c50000"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#c50000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#c50000"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#c50000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#c50000"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#c50000"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#c50000"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#c50000"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#c50000"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}]
  });
  var mapLoadListener = map.addListener('tilesloaded', function() {
    didLoadHandler();
    google.maps.event.removeListener(mapLoadListener);
  });
};

function pinIcon(url, scale) {
  var width = 50 * scale;
  var height = 58 * scale;
  return {
    url: url,
    anchor: new google.maps.Point(width / 2, height / 1),
    scaledSize: new google.maps.Size(width, height)
  };
}

function setupKitsunePins(json) {
  var kitsunesButtonCount = document.getElementById("controlKitsunesButtonCount");
  kitsunesButtonCount.innerHTML = numberWithCommas(json.length) + " ";
  json.forEach(function(pin) {
    var location = {lat: pin.lat, lng: pin.lng};
    var imageURL = '/images/pins/' + pin.icon + '.png?v=' + versionIdentifier();
    var image = pinIcon(imageURL, 0.5);
    var marker =  new google.maps.Marker({
      position: location,
      icon: image,
      map: map,
    });
    marker.addListener('click', function() {
      getKitsunePinContent(pin.id, function(html) {
        showInfoWindow(html, marker);
      });
    });
    kitsuneMarkers.push(marker);
  });

  var lastZoom = startZoom;
  google.maps.event.addListener(map, 'zoom_changed', function() {
    var zoom = map.getZoom();
    var zoomSwitch = 9;
    if (zoom >= zoomSwitch && lastZoom < zoomSwitch) {
      kitsuneMarkers.forEach(function(marker) {
        var image = pinIcon(marker.icon.url, 1.0);
        marker.setIcon(image);
      });
    } else if (zoom <= zoomSwitch - 1 && lastZoom > zoomSwitch - 1) {
      kitsuneMarkers.forEach(function(marker) {
        var image = pinIcon(marker.icon.url, 0.5);
        marker.setIcon(image);
      });
    }
    lastZoom = zoom;
  });
}

function getNewPinLatLng() {
  var latlng = newPinMarker.getPosition();
  return {lat: latlng.lat(), lng: latlng.lng()};
}

function removeNewPinMarker() {
  markerInfoWindow.close();
  newPinMarker.setMap(null);
}

function dropPin(isJourney) {
  if (newPinMarker !== undefined) {
    newPinMarker.setMap(null);
  }
  var contentID = isJourney ? 'newJourneyPinMessage' : 'newPinMessage'
  newPinMarker = new google.maps.Marker({
    position: map.getCenter(),
    map: map,
    animation: google.maps.Animation.DROP,
    draggable:true
  });
  if (markerInfoWindow !== undefined) {
    markerInfoWindow.close()
  }
  markerInfoWindow = new google.maps.InfoWindow({
    content: document.getElementById(contentID).innerHTML
  })
  google.maps.event.addListener(markerInfoWindow, 'closeclick', function() {
    newPinMarker.setMap(null);
  });
  map.setZoom(12);
  markerInfoWindow.open(map, newPinMarker);
  document.getElementById(contentID).style.display = "block";
}

function journeyPinIcon(url, scale) {
  var width = 80 * scale;
  var height = 100 * scale;
  return {
    url: url,
    anchor: new google.maps.Point(width / 2, height / 2),
    scaledSize: new google.maps.Size(width, height)
  };
}

function removeAllPopups() {
  if (markerInfoWindow !== undefined) {
    markerInfoWindow.close();
  }
}

function showInfoWindow(html, marker) {
  removeAllPopups();
  markerInfoWindow = new google.maps.InfoWindow({
    content: html
  });
  markerInfoWindow.open(map, marker);
}

function setupJourneyLayer(json) {
  var journeyCoordinates = Array();
  var filterOption = document.getElementById('wonderfulJourneyOption');
  json.forEach(function(pin, index) {
    var location = {lat: pin.lat, lng: pin.lng};
    var pinURL = '/images/pins/journey/' + (index % 3) + '.png?v=' + versionIdentifier();
    var marker =  new google.maps.Marker({
      position: location,
      icon: journeyPinIcon(pinURL, 0.5),
      map: map,
      zIndex: 999999999,
    });
    journeyCoordinates.push(location)
    var showJourneyPinInfoWindow = function() {
      getJourneyPinContent(pin.id, function(html) {
        showInfoWindow(html, marker);
      });
    };
    marker.addListener('click', showJourneyPinInfoWindow);
    wonderfulJourneyMarkers.push(marker);

    var newLocation = document.getElementById('wonderfulJourneyLocation' + index);
    newLocation.onclick = function() {
      map.panTo(location);
      showJourneyPinInfoWindow();
    };
  });
  var lastZoom = startZoom;
  google.maps.event.addListener(map, 'zoom_changed', function() {
    var zoom = map.getZoom();
    var zoomSwitch = 9;
    if (zoom >= zoomSwitch && lastZoom < zoomSwitch) {
      wonderfulJourneyMarkers.forEach(function(marker) {
        var image = journeyPinIcon(marker.icon.url, 1.0);
        marker.setIcon(image);
      });
    } else if (zoom <= zoomSwitch - 1 && lastZoom > zoomSwitch - 1) {
      wonderfulJourneyMarkers.forEach(function(marker) {
        var image = journeyPinIcon(marker.icon.url, 0.5);
        marker.setIcon(image);
      });
    }
    lastZoom = zoom;
  });
  journeyPath = new google.maps.Polyline({
    path: journeyCoordinates,
    geodesic: true,
    strokeColor: '#e6df85',
    strokeOpacity: 1.0,
    strokeWeight: 4
  });
  journeyPath.setMap(map);
}

function setPinsVisibility(pins, isVisible) {
  pins.forEach(function(pin, index) {
    if (isVisible) {
      pin.setMap(map);
    } else {
      pin.setMap(null);
    }
  });
}

function toggleWonderfulJourney() {
  removeAllPopups();
  var isAlreadyVisible = wonderfulJourneyMarkers[0].getMap() != null;
  setPinsVisibility(wonderfulJourneyMarkers, !isAlreadyVisible);
  if (isAlreadyVisible) {
    journeyPath.setMap(null);
  } else {
    journeyPath.setMap(map);
  }
  setJourneyFilterSelected(!isAlreadyVisible);
}

function toggleKitsuneMarkers() {
  removeAllPopups();
  var isAlreadyVisible = kitsuneMarkers[0].getMap() != null;
  setPinsVisibility(kitsuneMarkers, !isAlreadyVisible);
  setKitsunePinsFilterSelected(!isAlreadyVisible);
}
