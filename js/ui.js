ready(function() {
  document.getElementById("bottomControl").addEventListener('touchmove', function(e) { e.preventDefault(); });
  document.getElementById("controlAddPinButton").addEventListener('click', function() {
    dropPin(false);
  });
  document.getElementById("controlMoreButton").addEventListener('click', showMore);
  var jounreyButtonElement = document.getElementById("controlAddJourneyPinButton");
  if (jounreyButtonElement !== null) {
    jounreyButtonElement.addEventListener('click', function() {
      dropPin(true);
    });
  }
});


function openModal(innerHTML) {
  document.getElementById('modal_content').innerHTML = innerHTML;
  document.getElementById('modal').style.display = "block";
}

function closeModal() {
  document.getElementById('modal').style.display = "none";
}

function toggleSideMenu() {
  var sideMenu = document.getElementById('sideMenu');
  if (sideMenu.classList.contains('showMenu')) {
    sideMenu.classList.remove('showMenu');
  } else {
    sideMenu.classList.add('showMenu');
  }
}

function setKitsunePinsFilterSelected(isSelected) {
  var classList = document.getElementById('kitsuneMarkersOption').classList;
  if (isSelected) {
    classList.add('active');
  } else {
    classList.remove('active');
  }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var loadingCount = 0;
function showLoadingIndicator() {
  loadingCount += 2;// once for actual show and once for min show timer
  var loadingImage = document.getElementById("loadingImage");
  loadingImage.style.display = "block";
  window.setTimeout(hideLoadingIndicator, 250);
}

function hideLoadingIndicator() {
  loadingCount--;
  if (loadingCount > 0) { return }
  var loadingImage = document.getElementById("loadingImage");
  loadingImage.style.display = "none";
  loadingCount = 0;
}

function openModalForm() {
  ajax({
    method: 'GET',
    relativeURL: '/new_pin_form.html',
    dataType: 'HTML',
    success: function(html) {
      openModal(html);
      setupPinInputRadios();
      addInputEventListeners();
    },
    error: function() {
      // There was a connection error of some sort
    }
  });
}

function setupPinInputRadios() {
  var pinInputs = document.querySelectorAll(".pin-input");
  Array.prototype.forEach.call(pinInputs, function(el, i){
    var pinInput = el.querySelectorAll("img")[0];
    var pinRadio = el.querySelectorAll("input")[0];
    pinInput.addEventListener("click", function() {
      pinRadio.checked = true;
    });
  });
}

function addInputEventListeners() {
  var inputs = document.querySelectorAll("input, select, textarea");
  Array.prototype.forEach.call(inputs, function(el, i){
    el.addEventListener("input", function () {
      var className = "has-error";
      if (el.value.length > 0) {
        className = "has-success";
      }
      if (el.type == "email") {
        if (validateEmail(el.value)) {
          className = "has-success";
        } else {
          className = "has-warning";
        }
      }
      var parent = el.parentNode;
      parent.classList.remove("has-warning");
      parent.classList.remove("has-error");
      parent.classList.remove("has-success");
      parent.classList.add(className);
    });
  });
}

function validateEmail(email) {
    var re = /^[a-z0-9_.-]+@[a-z0-9-]+\.[a-z0-9-.]+$/;
    return re.test(email.toLowerCase());
}

function getFormValue(elementID) {
  var element = document.getElementById(elementID);
  if (element != null) {
    return element.value;
  } else {
    return "";
  }
}

function getPinFormData() {
  var data = new QueryStringBuilder();
  data.add("name", document.getElementById('inputName').value);
  data.add("about_you", document.getElementById('inputAboutYou').value);
  data.add("email", getFormValue('inputEmail').toLowerCase());
  data.add("favorite_member", document.getElementById('inputFavMember').value);
  data.add("favorite_song", document.getElementById('inputFavSong').value);
  data.add("editAccessUUID", getFormValue('editAccessUUIDinput'));
  data.add("communities", getSelectValues(document.getElementById('inputCommunities')));
  if (newPinMarker !== undefined) {
    var latlng = getNewPinLatLng();
    data.add("latitude", latlng.lat);
    data.add("longitude", latlng.lng);
  }

  var pinRadios = document.getElementsByName('inputPinIcon');
  for (var i = 0, length = pinRadios.length; i < length; i++) {
      if (pinRadios[i].checked) {
          data.add("pin_icon", pinRadios[i].value);
          break;
      }
  }
  return data;
}

function submitNewPinForm() {
  var data = getPinFormData();
  ajax({
    method: 'POST',
    relativeURL: '/pin',
    dataType: 'HTML',
    data: data,
    success: function(html) {
      removeNewPinMarker();
      openModal(html);
    },
    error: function(html) {
      var formErrorElement = document.getElementById("formError");
      formErrorElement.innerHTML = html
    }
  });
}

function submitEditPinForm() {
  var data = getPinFormData();
  ajax({
    method: 'PUT',
    relativeURL: '/pin',
    dataType: 'HTML',
    data: data,
    success: function(html) {
      openModal(html);
    },
    error: function(html) {
      var formErrorElement = document.getElementById("formError");
      formErrorElement.innerHTML = html
    }
  });
}

function submitDeletePin() {
  var data = new QueryStringBuilder();
  data.add("editAccessUUID", document.getElementById('editAccessUUIDinput').value);
  ajax({
    method: 'DELETE',
    relativeURL: '/pin?' + data.toQueryString(),
    dataType: 'HTML',
    success: function(html) {
      openModal(html);
    },
    error: function(html) {
      var formErrorElement = document.getElementById("formError");
      formErrorElement.innerHTML = html
    }
  });
}

function submitEditPinEmail() {
  var data = new QueryStringBuilder();
  data.add("email", document.getElementById('inputEditEmail').value.toLowerCase());

  ajax({
    method: 'POST',
    relativeURL: '/pin/editRequest',
    dataType: 'HTML',
    data: data,
    success: function(html) {
      openModal(html);
    },
    error: function(html) {
      // There was a connection error of some sort
    }
  });

}

function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}

function showMore() {
  openModal(document.getElementById('aboutContent').innerHTML);
  addInputEventListeners();
  var editEmailInputListener = function() {
    submitEditPinEmail();
  }
  var editEmailInput = document.getElementById("inputEditEmail");
  editEmailInput.addEventListener("input", function() {
    var editEmailButton = document.getElementById("editEmailButton");
    editEmailButton.removeEventListener("click", editEmailInputListener);
    if (validateEmail(editEmailInput.value)) {
      editEmailButton.classList.remove("disabled");
      editEmailButton.addEventListener("click", editEmailInputListener);
    } else {
      editEmailButton.classList.add("disabled");
    }
  });
  var languageSelector = document.getElementById("languageSelector");
  languageSelector.addEventListener("input", function() {
    document.cookie = "language=" + languageSelector.value;
    location.reload();
  });
}

function getKitsunePinContent(pinID, completion) {
  ajax({
    method: 'GET',
    relativeURL: '/pin/' + pinID,
    dataType: 'HTML',
    success: completion,
    error: function() {
      // There was a connection error of some sort
    }
  });
}
