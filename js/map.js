var didLoad = false;

ready(function() {
  showLoadingIndicator();
  var japan = {lat: 36.2048, lng: 138.2529};
  setTimeout(checkLoaded, 20 * 1000);
  initMap(japan, function() {
    loadKitsunePins(function() {
      didLoad = true;
      loadWonderfulJourney();
      hideLoadingIndicator();
    });
  });
})

function checkLoaded() {
  if (!didLoad) {
    hideLoadingIndicator();
    openModal(document.getElementById('switchMapsContent').innerHTML);
  }
}

function switchMaps() {
  var param = window.location.search.length > 0 ? "" : "/?mapType=gmaps"
  var newLocation = window.location.protocol + "//" + window.location.host + param
  window.location = newLocation
}

function loadKitsunePins(completion) {
  var url = '/pins.json';
  if (document.getElementById('shouldForceRefreshPins')) {
    url += '?forceRefresh=True';
  }
  ajax({
    method: 'GET',
    relativeURL: url,
    dataType: 'JSON',
    success: function(json) {
      var kitsunesButtonCount = document.getElementById("controlKitsunesButtonCount");
      kitsunesButtonCount.innerHTML = numberWithCommas(json.length) + " ";
      setupKitsunePins(json);
      completion();
    },
    error: function() {
      // There was a connection error of some sort
    }
  });
}
