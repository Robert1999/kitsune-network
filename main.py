import webapp2
import os
import json
import base64
import uuid

from entities import Pin
from utilities import send_email
from utilities import KNRequestHandler
from utilities import get_translations
import constants
from constants import JINJA_ENVIRONMENT

import cloudstorage
from google.appengine.api import app_identity
from google.appengine.api import taskqueue

class MainHandler(KNRequestHandler):
    def get(self):
        template_values = {}
        show_modal_onload = False
        activate_pin_uuid = self.request.GET.get('activatePin')
        if activate_pin_uuid != None:
            if Pin.activate_pin(activate_pin_uuid):
                show_modal_onload = True
                template_values["show_pin_activated_message"] = True
                template_values["force_refresh_pins"] = True
            else:
                self.redirect('/')
                return
        edit_pin_uuid = self.request.GET.get('editPin')
        if edit_pin_uuid != None:
            edit_pin = Pin.query(Pin.access_uuid == edit_pin_uuid).get()
            if edit_pin != None:
                show_modal_onload = True
                template_values["edit_pin_uuid"] = edit_pin_uuid
                template_values["pin"] = edit_pin
                template_values["pin_communities"] = [int(x) for x in edit_pin.communities.split(',')]
                template_values["show_pin_edit_form"] = True
            else:
                self.redirect('/')
                return
        template_values["show_modal_onload"] = show_modal_onload
        template_values["map_type"] = self.request.GET.get('mapType')
        template_values["cookie_language"] = self.request.cookies.get('language')
        template_values["languages_dict"] = constants.languages
        template_values["languages_display_sort"] = constants.languages_display_sort
        template_values["journey_index"] = self.request.GET.get('journey_index')
        template = JINJA_ENVIRONMENT.get_template('templates/map.html')
        self.response.write(template.render(template_values))

class PinsHandler(webapp2.RequestHandler):
    def get(self):
        bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
        filename = '/' + bucket_name + '/' + constants.PIN_JSON_FILENAME
        with cloudstorage.open(filename) as cloudstorage_file:
            self.response.write(cloudstorage_file.read())

class NewPinFormHandler(KNRequestHandler):
    def get(self):
        template_values = {}
        template = JINJA_ENVIRONMENT.get_template('templates/pin/new_pin_form.html')
        self.response.write(template.render(template_values))

class PinInfoHandler(KNRequestHandler):
    def get(self, pin_id):
        pin = Pin.get_by_id(int(pin_id))
        if pin == None:
            self.response.set_status(404)
            self.response.out.write("")
            return
        communities = pin.communities.split(',')
        if len(communities) > 1 and '0' in communities:
            communities.remove('0')
        template_values = {
            'pin': pin,
            'fav_song': constants.songs[str(pin.favorite_song)],
            'fav_member': constants.members[str(pin.favorite_member)],
            'communities': [constants.communities[community] for community in communities],
        }
        template = JINJA_ENVIRONMENT.get_template('templates/pin_info_window.html')
        # self.response.headers["cache-control"] = "max-age=600"
        self.response.write(template.render(template_values))

class PinEditRequestHandler(KNRequestHandler):
    def post(self):
        email = self.request.POST.get('email')
        edit_pin = Pin.query(Pin.email == email).get()
        if edit_pin and edit_pin.is_activated:
            edit_pin.access_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')
            edit_pin.put()
            task = taskqueue.add(
                url = '/tasks/send_edit_email',
                params = { 'email': edit_pin.email, 'access_uuid': edit_pin.access_uuid })
        template_values = { 'action': 'edit' }
        template = JINJA_ENVIRONMENT.get_template('templates/email_sent.html')
        self.response.write(template.render(template_values))

class ManagePinHandler(KNRequestHandler):
    def delete(self):
        edit_pin_uuid = self.request.GET.get('editAccessUUID')
        pin = Pin.query(Pin.access_uuid == edit_pin_uuid).get()
        if pin == None:
            self.response.set_status(404)
            self.response.out.write("")
            return

        pin.delete()

        template = JINJA_ENVIRONMENT.get_template('templates/pin_deleted.html')
        self.response.write(template.render())

    def put(self):
        edit_pin_uuid = self.request.POST.get('editAccessUUID')
        pin = Pin.query(Pin.access_uuid == edit_pin_uuid).get()
        if pin == None:
            self.response.set_status(404)
            self.response.out.write("")
            return

        translations = get_translations(self.request)
        form_error_message = Pin.validate_pin_values(self.request.POST, False, translations)
        if form_error_message:
            self.response.set_status(400)
            self.response.out.write(form_error_message)
            return

        pin.set_pin_values(self.request.POST, self.request.remote_addr, False, translations)
        task = taskqueue.add(
            url = '/tasks/update_pin_json',
            params = {
                'pin_id': pin.key.id(),
                'pin_icon': pin.pin_icon,
            })
        pin.send_discord_moderation_web_hook()

        template = JINJA_ENVIRONMENT.get_template('templates/pin_updated.html')
        self.response.write(template.render())

    def post(self):
        translations = get_translations(self.request)
        form_error_message = Pin.validate_pin_values(self.request.POST, True, translations)
        if form_error_message:
            self.response.set_status(400)
            self.response.out.write(form_error_message)
            return

        new_pin = Pin()
        new_pin.set_pin_values(self.request.POST, self.request.remote_addr, True, translations)

        send_email(new_pin.email, new_pin.access_uuid, False)

        template_values = { 'action': 'activate' }
        template = JINJA_ENVIRONMENT.get_template('templates/email_sent.html')
        self.response.write(template.render(template_values))

class DiscordRedirectHandler(webapp2.RequestHandler):
    def get(self):
        self.redirect('https://discord.gg/QywBrcf')

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/discord', DiscordRedirectHandler),
    ('/pin', ManagePinHandler),
    ('/pin/editRequest', PinEditRequestHandler),
    ('/pin/(.*)', PinInfoHandler),
    ('/pins.json', PinsHandler),
    ('/new_pin_form.html', NewPinFormHandler),
], debug=True)
