import webapp2
import os
import json
import base64
import uuid

from entities import JourneyPin
from entities import Credential
from utilities import is_production
from utilities import KNRequestHandler
import constants
from constants import JINJA_ENVIRONMENT

import cloudstorage
from google.appengine.api import app_identity
from babel.support import Translations
from google.appengine.api import images

from main import MainHandler

class JourneyUpdatePinFormHandler(KNRequestHandler):
    def get(self, journey_pin_id):
        template_values = {}
        if journey_pin_id:
            journey_pin = JourneyPin.get_by_id(int(journey_pin_id))
            if journey_pin == None:
                self.response.set_status(404)
                self.response.out.write("")
                return
            template_values["journey_pin"] = journey_pin
            if journey_pin.reddit_url:
                template_values["reddit_url"] = journey_pin.reddit_url
            if journey_pin.youtube_id:
                template_values["youtube_id"] = journey_pin.youtube_id
        template = JINJA_ENVIRONMENT.get_template('templates/journey_update_pin_form.html')
        self.response.write(template.render(template_values))

class ManageJourneyHandler(MainHandler):
    def get(self, journey_index):
        self.request.GET['journey_index'] = journey_index if journey_index else '0'
        MainHandler.get(self)

    def process_journey_pin_values(self, is_new_pin):
        password = self.request.POST.get('password')
        if is_production() and password != Credential.get('WONDERFUL_JOURNEY_PASSWORD'):
            self.response.set_status(400)
            self.response.out.write("Invalid Password")
            return

        if is_new_pin:
            journey_pin = JourneyPin()
        else:
            journey_pin_id = self.request.POST.get('editJourneyPinID')
            journey_pin = JourneyPin.get_by_id(int(journey_pin_id))
            if journey_pin == None:
                self.response.set_status(404)
                self.response.out.write("")
                return

        try:
            journey_pin.set_pin_values(self.request.POST, self.request.remote_addr, is_new_pin)
            image = self.request.POST.get('image')
            if image:
                image_data = base64.b64decode(image)
                img_obj = images.Image(image_data)
                # https://cloud.google.com/appengine/docs/python/refdocs/modules/google/appengine/api/images#Image.get_original_metadata
                ALLOWED_IMAGE_FORMATS = {
                    images.JPEG: 'jpeg',
                    images.PNG:  'png',
                    images.WEBP: 'webp',
                    images.BMP:  'bmp',
                    images.GIF:  'gif',
                    # images.ICO:  'x-icon',
                }
                file_extension = ALLOWED_IMAGE_FORMATS[img_obj.format]
                bucket_name = os.environ.get('BUCKET_NAME',
                                             app_identity.get_default_gcs_bucket_name())
                filename = ("/" + bucket_name + "/journey_images/%i") % journey_pin.key.id()
                with cloudstorage.open(filename, 'w', content_type="image/" + file_extension) as cloudstorage_file:
                    cloudstorage_file.write(image_data)
            template = JINJA_ENVIRONMENT.get_template('templates/journey_pin_added.html')
            self.response.write(template.render())
        except:
            self.response.set_status(400)
            self.response.out.write("There was an error.")
            return

    def put(self, unused):
        self.process_journey_pin_values(False)

    def post(self, unused):
        self.process_journey_pin_values(True)

class JourneyPinInfoHandler(KNRequestHandler):
    def get(self, pin_id):
        journey_pin = JourneyPin.get_by_id(int(pin_id))
        if journey_pin == None:
            self.response.set_status(404)
            self.response.out.write("")
            return
        template_values = {
            'pin': journey_pin,
            'pin_id': pin_id
        }
        template = JINJA_ENVIRONMENT.get_template('templates/journey_pin_info_window.html')
        self.response.write(template.render(template_values))

class JourneyImageHandler(webapp2.RequestHandler):
    def get(self, pin_id):
        try:
            bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
            filename = "/" + bucket_name + "/journey_images/" + pin_id
            with cloudstorage.open(filename) as cloudstorage_file:
                cloudstorage_stat = cloudstorage.stat(filename)
                self.response.headers['Content-Type'] = cloudstorage_stat.content_type
                self.response.write(cloudstorage_file.read())
        except:
            self.response.set_status(404)
            self.response.out.write("")


class JourneyPinsHandler(webapp2.RequestHandler):
    def get(self):
        pins_array = []
        for pin in JourneyPin.query().order(JourneyPin.created_datetime).fetch():
            pin_dict = {}
            pin_dict["id"] = pin.key.id()
            pin_dict["title"] = pin.name
            pin_dict["location"] = pin.formatted_address
            pin_dict["lat"] = pin.point.lat
            pin_dict["lng"] = pin.point.lon
            pins_array.append(pin_dict)
        self.response.headers["cache-control"] = "no-cache, no-store, must-revalidate"
        self.response.out.write(json.dumps(pins_array))

app = webapp2.WSGIApplication([
    ('/journey/pin/(.*)', JourneyPinInfoHandler),
    ('/journey/image/(.*)', JourneyImageHandler),
    ('/journey/journey_pins.json', JourneyPinsHandler),
    ('/journey/journey_update_pin_form/(.*)', JourneyUpdatePinFormHandler),
    ('/journey.?(.*)', ManageJourneyHandler),
], debug=True)
