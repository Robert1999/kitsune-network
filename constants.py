# -*- coding: utf-8 -*-
import jinja2
import os

version_identifier = '3.0.0-alpha.9'

PIN_JSON_FILENAME = 'pins.json'

pin_icons = [8,5,6,1,7]

members = {
    '0': "They are all my favorite",
    '1': 'スーメタル / SU-METAL / Queen Su'.decode('utf8'),
    '2': 'モアメタル / MOAMETAL / Angel of Love'.decode('utf8'),
    '3': 'ユイメタル / YUIMETAL / Angel of Dance'.decode('utf8'),
}
members_display_sort = range(1, 4)

songs = {
    "0": "They are all my favorite",
    "1": "Doki Doki ☆ Morning / ド・キ・ド・キ☆モーニング".decode('utf8'),
    "2": "Ijime Dame Zettai / イジメ、ダメ、ゼッタイ".decode('utf8'),
    "3": "Akatsuki / 紅月-アカツキ-".decode('utf8'),
    "4": "Iine! / いいね".decode('utf8'),
    "5": "Head Bangya!! / ヘドバンギャー! !".decode('utf8'),
    "6": "Uki Uki ★ Midnight / ウ・キ・ウ・キ★ミッドナイト".decode('utf8'),
    "7": "BABYMETAL DEATH",
    "8": "Onedari Daisakusen / おねだり大作戦".decode('utf8'),
    "9": "Catch me if you can",
    "10": "Megitsune / メギツネ".decode('utf8'),
    "11": "NO RAIN, NO RAINBOW",
    "12": "Gimme Chocolate!! / ギミチョコ！！".decode('utf8'),
    "13": "Rondo of Nightmare / 悪夢の輪舞曲".decode('utf8'),
    "14": "Song 4 / 4の歌".decode('utf8'),
    "15": "Road of Resistance",
    "16": "Awadama Fever / あわだまフィーバー".decode('utf8'),
    "17": "YAVA! / ヤバッ！".decode('utf8'),
    "18": "THE ONE",
    "19": "Karate",
    "20": "Amore / Amore - 蒼星 -".decode('utf8'),
    "21": "Meta Tarō / META！メタ太郎".decode('utf8'),
    "22": "Syncopation / シンコペーション".decode('utf8'),
    "23": "From Dusk Till Dawn",
    "24": "GJ!",
    "25": "Sis. Anger",
    "26": "Tales of The Destinies",
    "27": "Kimi to Anime Ga Mitai / 君とアニメが見たい".decode('utf8'),
    "28": "White Love",
    "29": "Over the future",
    "30": "Tsubasa wo kudasai",
    "31": "Chokotto Love",
    "32": "Love Machine",
    "33": "Soul's Refrain",
}
songs_display_sort = range(1, 34)

communities = {
    "0": "None",
    "1": "THE ONE",
    "2": "/r/BABYMETAL",
    "3": "/r/BABYMETAL_japan",
    "4": "vk.com/babymetal",
    "5": "BABYMETAL FAN CLUB",
    "6": "Baidu Tieba",
    "7": "weibo.com/babymetal",
}
communities_display_sort = range(1, 8)

languages = {
    'en': '🇺🇸 English'.decode('utf8'),
    'ja': '🇯🇵 日本語'.decode('utf8'),
    'ru': '🇷🇺 русский'.decode('utf8'),
    'de': '🇩🇪 Deutsch'.decode('utf8'),
    'nl': '🇳🇱 Nederlands'.decode('utf8'),
    'es': '🇪🇸 Español'.decode('utf8'),
    'no': '🇳🇴 norsk'.decode('utf8'),
    'fr': '🇫🇷 français'.decode('utf8'),
    'pt': '🇵🇹 portugues'.decode('utf8'),
    'pl': '🇵🇱 polski'.decode('utf8'),
    'id': '🇮🇩 Indonesia'.decode('utf8'),
    'it': '🇮🇹 italiana'.decode('utf8'),
    'zh': '🇨🇳 中文'.decode('utf8'),
    'fi': '🇫🇮 Suomen'.decode('utf8'),
}
languages_display_sort = ['en', 'ja', 'ru', 'de', 'nl', 'es', 'no', 'fr', 'pt', 'pl', 'id', 'it', 'zh', 'fi']

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape', 'jinja2.ext.i18n'],
    autoescape=True)

JINJA_ENVIRONMENT.globals["version_identifier"] = version_identifier
JINJA_ENVIRONMENT.globals["pin_icons"] = pin_icons
JINJA_ENVIRONMENT.globals["songs_dict"] = songs
JINJA_ENVIRONMENT.globals["songs_display_sort"] = songs_display_sort
JINJA_ENVIRONMENT.globals["members_dict"] = members
JINJA_ENVIRONMENT.globals["members_display_sort"] = members_display_sort
JINJA_ENVIRONMENT.globals["communities_dict"] = communities
JINJA_ENVIRONMENT.globals["communities_display_sort"] = communities_display_sort
